package rf.androidovshchik.button.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rf.androidovshchik.button.MainActivity;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.utils.AlarmUtil;
import rf.androidovshchik.button.utils.RandomUtil;
import rf.androidovshchik.button.utils.ServiceUtil;
import timber.log.Timber;

public class ActivityTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("ActivityTrigger: received");
		Prefs prefs = new Prefs(context);
		ServiceUtil.launchEmailSend(context, false, prefs);
		if (prefs.getBoolean(Prefs.RUN_BUTTON)) {
			AlarmUtil.next(context, RandomUtil.timeDelay(prefs), getClass());
			Intent activity = new Intent(context, MainActivity.class);
			activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			activity.putExtra(MainActivity.EXTRA_RUN_NOW, true);
			context.startActivity(activity);
		}
	}
}
