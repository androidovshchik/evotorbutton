package rf.androidovshchik.button.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rf.androidovshchik.button.data.DbManager;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.rows.Row;
import rf.androidovshchik.button.rows.Statistics;
import timber.log.Timber;

public class ShutdownTrigger extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("ShutdownTrigger: received " + intent.getAction());
        Prefs prefs = new Prefs(context);
        if (prefs.getBoolean(Prefs.RUN_BUTTON)) {
            DbManager manager = new DbManager(context);
            manager.onInsertRow(new Statistics(Statistics.EVENT_TURNED_OFF))
                    .subscribe((Row row) -> manager.onInsertRow(new Statistics(Statistics.EVENT_TURNED_ON, 0))
                            .subscribe());
        }
    }
}