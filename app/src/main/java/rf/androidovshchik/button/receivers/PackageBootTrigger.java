package rf.androidovshchik.button.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rf.androidovshchik.button.data.DbManager;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.utils.AlarmUtil;
import rf.androidovshchik.button.utils.RandomUtil;
import timber.log.Timber;

public class PackageBootTrigger extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("PackageBootTrigger: received " + intent.getAction());
        Prefs prefs = new Prefs(context);
        if (prefs.getBoolean(Prefs.RUN_BUTTON)) {
            AlarmUtil.next(context, RandomUtil.timeDelay(prefs), getClass());
        }
        DbManager manager = new DbManager(context);
        manager.onExecSql("UPDATE statistics SET timestamp = " + System.currentTimeMillis() +
                " WHERE timestamp = 0;").subscribe();
    }
}