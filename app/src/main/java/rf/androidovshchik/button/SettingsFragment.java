package rf.androidovshchik.button;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.design.widget.Snackbar;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.button.data.DbManager;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.rows.Row;
import rf.androidovshchik.button.rows.Statistics;
import rf.androidovshchik.button.utils.AlarmUtil;
import rf.androidovshchik.button.utils.NetworkUtil;
import rf.androidovshchik.button.utils.RandomUtil;
import rf.androidovshchik.button.utils.ServiceUtil;
import timber.log.Timber;

public class SettingsFragment extends PreferenceFragment {

    private ConnectivityManager connectivityManager;

    private CompositeDisposable disposable = new CompositeDisposable();

    private DbManager manager;

    private Prefs prefs;

    private Preference runButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_settings);
        manager = new DbManager(getActivity().getApplicationContext());
        prefs = new Prefs(getActivity().getApplicationContext());
        connectivityManager = (ConnectivityManager) getActivity()
            .getSystemService(Context.CONNECTIVITY_SERVICE);
        runButton = findPreference(Prefs.RUN_BUTTON);
        runButton.setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
            Timber.d("run: %b", (Boolean) newValue);
            runButton.setEnabled(false);
            manager.onInsertRow(new Statistics((Boolean) newValue ?
                Statistics.EVENT_BUTTON_ENABLED : Statistics.EVENT_BUTTON_DISABLED))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Row row) -> {
                    runButton.setEnabled(true);
                    if (!(Boolean) newValue) {
                        AlarmUtil.cancel(getActivity().getApplicationContext());
                    } else {
                        AlarmUtil.next(getActivity().getApplicationContext(),
                            RandomUtil.timeDelay(prefs), getClass());
                    }
                });
            return true;
        });
        findPreference(Prefs.MIN_DELAY)
            .setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
                Timber.d("min: %d", (Integer) newValue);
                if (prefs.getBoolean(Prefs.RUN_BUTTON)) {
                    prefs.putInt(Prefs.MIN_DELAY, (Integer) newValue);
                    AlarmUtil.next(getActivity().getApplicationContext(), RandomUtil.timeDelay(prefs), getClass());
                    return false;
                }
                return true;
            });
        findPreference(Prefs.MAX_DELAY)
            .setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
                Timber.d("max: %d", (Integer) newValue);
                if (prefs.getBoolean(Prefs.RUN_BUTTON)) {
                    prefs.putInt(Prefs.MAX_DELAY, (Integer) newValue);
                    AlarmUtil.next(getActivity().getApplicationContext(), RandomUtil.timeDelay(prefs), getClass());
                    return false;
                }
                return true;
            });
        findPreference(Prefs.TIME_PRESS)
            .setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
                Timber.d("press: %d", (Integer) newValue);
                int minDelay = Math.abs(prefs.getInt(Prefs.MIN_DELAY));// minutes
                int maxDelay = Math.abs(prefs.getInt(Prefs.MAX_DELAY));// minutes
                if ((Integer) newValue / 60 >= Math.min(minDelay, maxDelay)) {
                    prefs.putInt(Prefs.TIME_PRESS, Prefs.DEFAULT_TIME);
                } else {
                    prefs.putInt(Prefs.TIME_PRESS, (Integer) newValue);
                }
                return false;
            });
        findPreference(Prefs.SEND_NOW)
                .setOnPreferenceClickListener((Preference preference) -> {
                    if (!NetworkUtil.isConnected(connectivityManager)) {
                        showMessage("Нет подключения к интернету");
                        return false;
                    }
                    Context context = getActivity().getApplicationContext();
                    Prefs prefs = new Prefs(context);
                    if (!prefs.has(Prefs.EMAIL_TO)) {
                        showMessage("Не задан e-mail для отправки");
                        return false;
                    }
                    if (disposable.size() > 0) {
                        showMessage("Попробуйте через 1 минуту");
                        return false;
                    }
                    Timber.d("Start waiting timeout");
                    disposable.add(Observable.just(true).delay(60, TimeUnit.SECONDS)
                            .subscribe((Boolean value) -> {
                                disposable.clear();
                                Timber.d("Finish waiting timeout");
                            }));
                    ServiceUtil.launchEmailSend(context, true, prefs);
                    return true;
                });
    }

    private void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
            message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}