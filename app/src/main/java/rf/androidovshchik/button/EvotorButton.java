package rf.androidovshchik.button;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.google.gson.GsonBuilder;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.remote.UniSender;
import timber.log.Timber;

@ReportsCrashes(mailTo = "@",
	customReportContent = {
		ReportField.APP_VERSION_CODE,
		ReportField.APP_VERSION_NAME,
		ReportField.ANDROID_VERSION,
		ReportField.PHONE_MODEL,
		ReportField.BRAND,
		ReportField.PRODUCT,
		ReportField.USER_COMMENT,
		ReportField.USER_APP_START_DATE,
		ReportField.USER_CRASH_DATE,
		ReportField.STACK_TRACE,
		ReportField.LOGCAT
	},
	mode = ReportingInteractionMode.DIALOG,
	resDialogText = R.string.error_crash,
	resDialogCommentPrompt = R.string.error_comment,
	resDialogTheme = R.style.AppTheme_Dialog)
public class EvotorButton extends Application {

	public static UniSender API;

	@Override
	public void onCreate() {
		super.onCreate();
		if (BuildConfig.DEBUG) {
			Stetho.initializeWithDefaults(getApplicationContext());
		} else {
			ACRA.init(this);
		}
		Prefs prefs = new Prefs(getApplicationContext());
		if (!prefs.has(Prefs.MIN_DELAY)) {
			prefs.putInt(Prefs.MIN_DELAY, Prefs.DEFAULT_MIN);
		}
		if (!prefs.has(Prefs.MAX_DELAY)) {
			prefs.putInt(Prefs.MAX_DELAY, Prefs.DEFAULT_MAX);
		}
		if (!prefs.has(Prefs.TIME_PRESS)) {
			prefs.putInt(Prefs.TIME_PRESS, Prefs.DEFAULT_TIME);
		}
		Timber.plant(new Timber.DebugTree());
		prefs.printAll();
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.hostnameVerifier((String hostname, SSLSession session) -> true);
		httpClient.addInterceptor(logging);
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(UniSender.URL)
				.addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
						.create()))
				.client(httpClient.build())
				.build();
		API = retrofit.create(UniSender.class);
	}
}