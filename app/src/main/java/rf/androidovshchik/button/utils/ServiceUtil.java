package rf.androidovshchik.button.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import rf.androidovshchik.button.MainService;
import rf.androidovshchik.button.data.Prefs;
import timber.log.Timber;

public class ServiceUtil {

    @SuppressWarnings("all")
    public static boolean isRunning(Context context, Class<? extends Service> clss) {
        ActivityManager manager = (ActivityManager)
            context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (clss.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean launchEmailSend(Context context, boolean force, Prefs prefs) {
        return launchService(context, force, prefs, Prefs.LAST_EMAIL_SEND,
            MainService.class, MainService.INTERVAL);
    }

    private static boolean launchService(Context context, boolean force, Prefs prefs, String pref,
                                         Class<? extends Service> clss, long interval) {
        long now = System.currentTimeMillis();
        long delay = prefs.getLong(pref);
        Timber.d(clss.getSimpleName() + ": delay is " + delay);
        Intent intent = ServiceUtil.getIntent(context, clss);
        if (ServiceUtil.isRunning(context, clss)) {
            if (now - delay < interval * 0.9) {
                return false;
            } else {
                context.stopService(intent);
            }
        }
        if (now - delay > interval * 0.9 || force) {
            Timber.d(clss.getSimpleName() + ": passed");
            prefs.putLong(pref, now);
            startServiceRightWay(context, intent);
            return true;
        }
        return false;
    }

    public static Intent getIntent(Context context, Class<? extends Service> clss) {
        return new Intent(context, clss);
    }

    public static void startServiceRightWay(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }
}
