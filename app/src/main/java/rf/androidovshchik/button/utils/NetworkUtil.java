package rf.androidovshchik.button.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    @SuppressWarnings("all")
    public static boolean isConnected(ConnectivityManager connectivityManager) {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}