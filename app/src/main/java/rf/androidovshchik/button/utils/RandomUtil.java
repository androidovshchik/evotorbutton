package rf.androidovshchik.button.utils;

import java.security.SecureRandom;

import rf.androidovshchik.button.MainService;
import rf.androidovshchik.button.data.Prefs;

public class RandomUtil {

    private static final SecureRandom RANDOM = new SecureRandom();

    public static long timeDelay(Prefs prefs) {
        int minXml = Math.abs(prefs.getInt(Prefs.MIN_DELAY));
        int maxXml = Math.abs(prefs.getInt(Prefs.MAX_DELAY));
        int minMath = Math.min(minXml, maxXml);
        int maxMath = Math.max(minXml, maxXml);
        if (maxMath - minMath <= 0) {
            minMath = Prefs.DEFAULT_MIN;
            maxMath = Prefs.DEFAULT_MAX;
        }
        return (RANDOM.nextInt(maxMath - minMath + 1) + minMath) * MainService.MINUTE;
    }
}
