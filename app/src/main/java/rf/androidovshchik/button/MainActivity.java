package rf.androidovshchik.button;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.button.data.DbManager;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.rows.Row;
import rf.androidovshchik.button.rows.Statistics;
import rf.androidovshchik.button.utils.AlarmUtil;
import rf.androidovshchik.button.utils.RandomUtil;
import ru.evotor.framework.users.User;
import ru.evotor.framework.users.UserApi;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_RUN_NOW = "runNow";

    private AudioManager audioManager;

    private CompositeDisposable disposable = new CompositeDisposable();

    private Prefs prefs;

    private DbManager manager;

    private Long idAsNotHere = null;

    private RingtonePlayer ringtonePlayer;

    private PowerManager.WakeLock wakeLock;

    @BindView(R.id.report)
    ImageView report;
    @BindView(R.id.settings)
    ImageView settings;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.timer)
    TextView timer;

    @Override
    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
            PowerManager.ACQUIRE_CAUSES_WAKEUP, getString(R.string.app_name));
        wakeLock.acquire();
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        manager = new DbManager(getApplicationContext());
        prefs = new Prefs(getApplicationContext());
        ringtonePlayer = new RingtonePlayer();
        if (prefs.getBoolean(Prefs.RUN_BUTTON) && getIntent().hasExtra(EXTRA_RUN_NOW)) {
            fab.setEnabled(true);
            fab.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccent));
            startCountDown();
        } else {
            fab.setEnabled(false);
            fab.setBackgroundTintList(getResources().getColorStateList(R.color.colorDisabled));
        }
        if (!prefs.has(Prefs.EMAIL_TO) || TextUtils.isEmpty(prefs.getString(Prefs.EMAIL_TO))) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                "В настройках не задан e-mail", Snackbar.LENGTH_SHORT);
            snackbar.setAction("Задать", (View view) -> startActivity(new Intent(getApplicationContext(),
                SettingsActivity.class)));
            snackbar.show();
        }
        User user = UserApi.getAuthenticatedUser(getApplicationContext());
        try {
            if (user.getRoleTitle().contains("Кассир")) {
                settings.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (idAsNotHere != null) {
            manager.onDeleteRow(Statistics.TABLE, idAsNotHere)
                .subscribe((Integer result) -> {
                    idAsNotHere = null;
                    AlarmUtil.cancel(getApplicationContext());
                });
        }
        report.setVisibility(prefs.getString(Prefs.EMAIL_TO).equals("gimrcpp@gmail.com") ?
            View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.report)
    void onReport() {
        Toast.makeText(getApplicationContext(), "Подождите...",
            Toast.LENGTH_SHORT).show();
        disposable.add(Observable.fromCallable(() -> {
            StringBuilder appLogs = new StringBuilder();
            try {
                Process process = Runtime.getRuntime().exec("logcat -d");
                BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    appLogs.append(line);
                    appLogs.append("\n");
                }
                Runtime.getRuntime().exec("logcat -c");
            } catch (Exception e) {
                Timber.e(e);
                return "";
            }
            return appLogs.toString();
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::sendEmail, (Throwable throwable) -> Timber.e(throwable)));
    }

    @OnClick(R.id.settings)
    void onSettings() {
        if (!fab.isEnabled()) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        }
    }

    @OnClick(R.id.fab)
    void onFab() {
        fab.setEnabled(false);
        disposable.clear();
        manager.onInsertRow(new Statistics(Statistics.EVENT_HERE))
            .subscribe((Row row) -> {
                AlarmUtil.next(getApplicationContext(), RandomUtil.timeDelay(prefs), getClass());
                finishAndRemoveTask();
            });
    }

    private void startCountDown() {
        timer.setText("");
        timer.setVisibility(View.VISIBLE);
        playRingtone();
        int timePress = prefs.getInt(Prefs.TIME_PRESS);
        disposable.add(Observable.interval(0, 1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((Long value) -> {
                if (value >= timePress + 2) {
                    finishAndRemoveTask();
                }
                if (value == timePress) {
                    if (idAsNotHere != null) {
                        finishAndRemoveTask();
                    } else if (fab.isEnabled()) {
                        fab.setEnabled(false);
                        manager.onInsertRow(new Statistics(Statistics.EVENT_NOT_HERE))
                            .subscribe((Row row) -> {
                                AlarmUtil.next(getApplicationContext(), AlarmUtil.MINUTE, getClass());
                                finishAndRemoveTask();
                            });
                    }
                }
                if (value == timePress / 2 && timePress >= 4) {
                    playRingtone();
                }
                timer.setText(String.valueOf(timePress - value));
            }));
    }

    private void playRingtone() {
        // audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
        //    audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        ringtonePlayer.play(getApplicationContext(), R.raw.marble);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (fab.isEnabled()) {
            manager.onInsertRow(new Statistics(Statistics.EVENT_NOT_HERE))
                .subscribe((Row row) -> {
                    idAsNotHere = row.id;
                    AlarmUtil.next(getApplicationContext(), AlarmUtil.MINUTE, getClass());
                });
        }
    }

    private void sendEmail(String logs) {
        if (logs.trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Нет новых логов для отправки",
                Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {
            "gimrcpp@gmail.com"
        });
        intent.putExtra(Intent.EXTRA_SUBJECT, "Логи приложения");
        intent.putExtra(Intent.EXTRA_TEXT, logs);
        try {
            startActivity(Intent.createChooser(intent, "Отправить письмо через приложение"));
        } catch (ActivityNotFoundException e) {
            Timber.e(e);
            Toast.makeText(getApplicationContext(), "Не удалось найти приложение для отправки",
                Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
        wakeLock.release();
    }
}
