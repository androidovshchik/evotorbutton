package rf.androidovshchik.button;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.squareup.sqlbrite3.BriteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rf.androidovshchik.button.data.DbManager;
import rf.androidovshchik.button.data.Prefs;
import rf.androidovshchik.button.receivers.ToastTrigger;
import rf.androidovshchik.button.remote.UniSender;
import rf.androidovshchik.button.rows.Row;
import rf.androidovshchik.button.rows.Statistics;
import rf.androidovshchik.button.rows.UniResponse;
import rf.androidovshchik.button.rx.Subscriber;
import rf.androidovshchik.button.utils.NetworkUtil;
import timber.log.Timber;

public class MainService extends Service {

	@SuppressWarnings("all")
	private static final SimpleDateFormat simpleDateFormat =
		new SimpleDateFormat("dd.MM.yy HH:mm");

	public static final long MINUTE = 60 * 1000L;

	public static final long HOUR = 60 * MINUTE;

	public static final long INTERVAL = 24 * HOUR;

	protected ConnectivityManager connectivityManager;

	protected CompositeDisposable disposable;

	protected DbManager manager;

	protected Prefs prefs;

	@SuppressWarnings("all")
	private NotificationManager notificationManager;

	private PowerManager.WakeLock wakeLock;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getString(R.string.app_name));
		wakeLock.acquire();
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(getString(rf.androidovshchik.button.R.string.app_name),
				getString(rf.androidovshchik.button.R.string.app_name), NotificationManager.IMPORTANCE_LOW);
			notificationManager.createNotificationChannel(channel);
		}
		connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		disposable = new CompositeDisposable();
		manager = new DbManager(getApplicationContext());
		prefs = new Prefs(getApplicationContext());
		onStartForeground(101, "Отправка отчета", rf.androidovshchik.button.R.drawable.ic_cloud_white_24dp);
	}

	@SuppressWarnings("all")
	protected void onStartForeground(int id, String title, @DrawableRes int image) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(rf.androidovshchik.button.R.string.app_name))
			.setSmallIcon(image)
			.setContentTitle(title)
			.setSound(null);
		startForeground(id, builder.build());
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!onCheckConditions()) {
			Timber.w(getClass().getSimpleName() + ": hasn't conditions");
			prefs.putLong(Prefs.LAST_EMAIL_SEND, 0L);
			onStopSync();
		} else {
			disposable.add(Observable.fromCallable(() -> {
				ArrayList<Statistics> statistics = new ArrayList<>();
				BriteDatabase.Transaction transaction = manager.db.newTransaction();
				try {
					statistics.addAll(Row.getRows(manager.db.query("SELECT * FROM statistics" +
						" WHERE sent = 0 LIMIT 1000"), Statistics.class));
					transaction.markSuccessful();
				} finally {
					transaction.end();
				}
				return statistics;
			}).subscribeOn(Schedulers.io())
				.subscribeWith(new Subscriber<ArrayList<Statistics>>() {

					@Override
					public void onNext(ArrayList<Statistics> statistics) {
						processStatistics(statistics);
					}

					@Override
					public void onError(Throwable e) {
						onShowMessage(e.getLocalizedMessage());
						onStopSync();
					}
				}));
		}
		return START_NOT_STICKY;
	}

	private void processStatistics(ArrayList<Statistics> statistics) {
		if (statistics.isEmpty()) {
			onShowMessage("нет данных для отправки");
			onStopSync();
			return;
		}
		if (!onCheckConditions()) {
			Timber.w("Hasn't conditions to continue");
			prefs.putLong(Prefs.LAST_EMAIL_SEND, 0L);
			onStopSync();
			return;
		}
		Timber.d("Processing statistics");
		Date date = new Date();
		StringBuilder report = new StringBuilder();
		String information = prefs.getString(Prefs.INFORMATION);
		if (!TextUtils.isEmpty(information)) {
			report.append(information);
			report.append("</br>");
		}
		for (int i = 0; i < statistics.size(); i++) {
			date.setTime(statistics.get(i).timestamp);
			report.append(simpleDateFormat.format(date));
			report.append(" ");
			report.append(statistics.get(i).event);
			report.append("</br>");
		}
		date.setTime(System.currentTimeMillis());
		sendEmail("Отчет от " + simpleDateFormat.format(date), report.toString(),
			statistics.get(statistics.size() - 1).id);
	}

	protected boolean onCheckConditions() {
		return NetworkUtil.isConnected(connectivityManager) && prefs.has(Prefs.EMAIL_TO);
	}

	private void sendEmail(String subject, String body, long lastId) {
		Timber.d("Sending email");
		HashMap<String, Object> params = new HashMap<>();
		params.put("api_key", UniSender.API_KEY);
		params.put("email", prefs.getString(Prefs.EMAIL_TO));
		params.put("sender_name", "Приложение \"Кнопка\"");
		params.put("sender_email", "button@evotorapp.ru");
		params.put("subject", subject);
		params.put("body", body);
		params.put("list_id", 12616773);
		EvotorButton.API.sendEmail(params)
				.enqueue(new Callback<UniResponse>() {

					@Override
					@SuppressWarnings("all")
					public void onResponse(Call<UniResponse> call, Response<UniResponse> response) {
						Timber.d("onResponse");
						UniResponse uniResponse = response.body();
						if (uniResponse == null || uniResponse.error != null) {
							prefs.putLong(Prefs.LAST_EMAIL_SEND, 0L);
							onShowMessage(uniResponse == null ? "неизвестная ошибка" : uniResponse.error);
							onStopSync();
						} else if (uniResponse.warnings != null) {
							prefs.putLong(Prefs.LAST_EMAIL_SEND, 0L);
							onShowMessage("указанный e-mail отписан от отчётов");
							onStopSync();
						} else {
							Timber.d("Success callback");
							manager.onExecSql("UPDATE statistics SET sent = 1 WHERE id <= " + lastId)
									.subscribe((Boolean value) -> {
										onShowMessage("отчёт отправлен только что");
										onStopSync();
									});
						}
					}

					@Override
					@SuppressWarnings("all")
					public void onFailure(Call<UniResponse> call, Throwable throwable) {
						Timber.e(throwable);
						prefs.putLong(Prefs.LAST_EMAIL_SEND, 0L);
						onShowMessage("не удалось отправить отчёт");
						onStopSync();
					}
				});
	}

	protected void onShowMessage(String message) {
		if (message == null) {
			message = "Nullable error";
		}
		Timber.i(message);
		Intent intent = new Intent(getPackageName() + ".TOAST");
		intent.putExtra(ToastTrigger.EXTRA_MESSAGE, getString(R.string.app_name) + ": "  + message);
		sendBroadcast(intent);
	}

	protected void onStopSync() {
		stopForeground(true);
		stopSelf();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		disposable.dispose();
		wakeLock.release();
	}
}
