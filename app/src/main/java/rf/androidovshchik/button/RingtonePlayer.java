package rf.androidovshchik.button;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.RawRes;

public class RingtonePlayer {

    private MediaPlayer mediaPlayer;

    @SuppressWarnings("all")
    public void play(Context context, @RawRes int raw) {
        stop();
        mediaPlayer = MediaPlayer.create(context, raw);
        mediaPlayer.setOnCompletionListener((MediaPlayer mediaPlayer) -> stop());
        mediaPlayer.start();
    }

    private void stop() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}