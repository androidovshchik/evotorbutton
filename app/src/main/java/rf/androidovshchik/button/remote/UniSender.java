package rf.androidovshchik.button.remote;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rf.androidovshchik.button.rows.UniResponse;

public interface UniSender {

    String URL = "https://api.unisender.com/ru/api/";

    String API_KEY = "66j1xmogjjqxueu8xdsoesc1pb5in35giknqx5pe";

    @FormUrlEncoded
    @POST("sendEmail?format=json")
    Call<UniResponse> sendEmail(@FieldMap HashMap<String, Object> params);
}
