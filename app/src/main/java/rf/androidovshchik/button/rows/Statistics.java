package rf.androidovshchik.button.rows;

import android.content.ContentValues;
import android.database.Cursor;

public class Statistics extends Row {

	public static final String TABLE = "statistics";

	public static final String EVENT_TURNED_ON = "Касса включена";
	public static final String EVENT_TURNED_OFF = "Касса отключена";
	public static final String EVENT_BUTTON_ENABLED = "Кнопка активирована";
	public static final String EVENT_BUTTON_DISABLED = "Кнопка деактивирована";
	public static final String EVENT_HERE = "На рабочем месте";
	public static final String EVENT_NOT_HERE = "Отсутствует";

	private static final String COLUMN_ID = "id";
	private static final String COLUMN_TIMESTAMP = "timestamp";
	private static final String COLUMN_EVENT = "event";

	public long timestamp;

	public String event;

	@SuppressWarnings("all")
	public Statistics() {}

	public Statistics(String event) {
		this.event = event;
		this.timestamp = System.currentTimeMillis();
	}

	public Statistics(String event, long timestamp) {
		this.event = event;
		this.timestamp = timestamp;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id != NONE) {
			values.put(COLUMN_ID, id);
		}
		values.put(COLUMN_TIMESTAMP, timestamp);
		values.put(COLUMN_EVENT, event);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIMESTAMP));
		event = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_EVENT));
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "Statistics{" +
			"id=" + id +
			", timestamp=" + timestamp +
			", event='" + event + '\'' +
			'}';
	}
}
