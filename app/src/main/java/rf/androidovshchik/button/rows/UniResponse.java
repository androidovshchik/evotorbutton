package rf.androidovshchik.button.rows;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Nullable;

public class UniResponse {

    @Nullable
    @SerializedName("error")
    public String error;

    @Nullable
    @SerializedName("warnings")
    public Object warnings;
}
